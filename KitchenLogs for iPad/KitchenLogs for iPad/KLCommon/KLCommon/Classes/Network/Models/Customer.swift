//
//  Customer.swift
//  KitchenLogs
//
//  Created by Viren Bhandari on 9/8/17.
//  Copyright © 2017 KitchenLogs. All rights reserved.
//

import UIKit
import Gloss

public class Customer: Encodable, Decodable {
//    "subscriptions": {
//    "url": "/v1/customers/cus_BH1FWdrS6vJYJk/subscriptions",
//    "total_count": 0,
//    "has_more": false,
//    "data": [],
//    "object": "list"
//    },
//    "sources": {
//    "url": "/v1/customers/cus_BH1FWdrS6vJYJk/sources",
//    "total_count": 1,
//    "has_more": false,
//    "data": [
//    {
//    "tokenization_method": null,
//    "name": "gautam@kitchenlogs.com",
//    "last4": "4242",
//    "funding": "credit",
//    "fingerprint": "YzZ8iUq2ewKZxbgg",
//    "exp_year": 2029,
//    "exp_month": 12,
//    "dynamic_last4": null,
//    "cvc_check": "pass",
//    "customer": "cus_BH1FWdrS6vJYJk",
//    "country": "US",
//    "brand": "Visa",
//    "address_zip_check": "pass",
//    "address_zip": "AMD 123",
//    "address_state": null,
//    "address_line2": null,
//    "address_line1_check": null,
//    "address_line1": null,
//    "address_country": null,
//    "address_city": null,
//    "object": "card",
//    "id": "card_1AuTCmCccLu4ajP4FO8rzwfp"
//    }
//    ],
//    "object": "list"
//    },
//    "shipping": null,
//    "livemode": false,
//    "email": "gautam@kitchenlogs.com",
//    "discount": null,
//    "description": null,
//    "delinquent": false,
//    "default_source": "card_1AuTCmCccLu4ajP4FO8rzwfp",
//    "currency": null,
//    "created": 1503611380,
//    "account_balance": 0,
//    "object": "customer",
//    "id": "cus_BH1FWdrS6vJYJk"

    
    init() {
    }
    
    public required init?(json: JSON) {
        
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            
            ])
    }
}
