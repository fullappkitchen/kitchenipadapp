//
//  LoginPost.swift
//  KitchenLogs
//
//  Created by Viren Bhandari on 9/8/17.
//  Copyright © 2017 KitchenLogs. All rights reserved.
//

import UIKit
import Gloss

open class LoginPost: Decodable, Encodable {
    
    public var email : String?
    public var password : String?
    public var token : String?
    
    init() {
        
    }
    
    public init(email:String, password:String, token:String) {
        self.email = email
        self.token = token
        self.password = password
    }
    
    public required init?(json: JSON) {
        self.email = "email" <~~ json
        self.password = "password" <~~ json
        self.token = "token" <~~ json
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "email" ~~> self.email,
            "password" ~~> self.password,
            "token" ~~> self.token
            ])
    }
}
