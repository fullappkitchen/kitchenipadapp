//
//  ApiErrorHandler.swift
//  Pods
//
//  Created by Viren Bhandari on 09/06/2017.
//
//


import Foundation
import UIKit
import Gloss

public enum ApiExceptionType : String {
    
    case InternalServer
    
    case AccessDenied
    
    case InvalidOTP

    case ExpiredOTP
    
    case InvalidToken
    
    case ExpiredToken
    
    case InvalidTenantDevice
    
    case OperationNotAllowed
    
    case IncompatibleVersion
    
    case UnkownException
    
    case InvalidDevice

}

open class ApiErrorHandler: Decodable {
    
    public var exceptionType: ApiExceptionType?
    
    public var message : String?
    
    public var errorCode : Int?
    
    public init() {}
    
    public required init?(json: JSON)
    {
        self.exceptionType = "ExceptionType" <~~ json
        self.message = "Message" <~~ json
    }
    
    public required init?(errorMessage: String?, errorCode: Int? = -5000, exceptionType: ApiExceptionType? = .UnkownException){
        self.message = errorMessage
        self.errorCode = errorCode
        self.exceptionType = exceptionType
    }
}
