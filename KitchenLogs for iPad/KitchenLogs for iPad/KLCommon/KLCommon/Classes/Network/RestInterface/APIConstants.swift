//
//  APIConstants.swift
//  Pods
//
//  Created by Viren Bhandari on 09/06/2017.
//
//

import UIKit

open class APIConstants: NSObject {

    open static let HeaderAuthorization: String = "Authorization"
    open static let HeaderAccept: String = "Accept"
    open static let HeaderContentType: String = "Content-Type"
    open static let MimeTypeJSON: String = "application/json"
    
    open static let NoInternetConnectivity: Int = 601
    open static let IncorrectURL: Int = 457
    open static let AuthenticationError: Int = 403
    open static let UnknownError: Int = 77
    open static let AdsRefreshDuration: Int = 15
    open static let ServerInternalError: Int = 500
    open static let BadRequest: Int = 400
    
    open static let ACCESS_TOKEN_EXPIRED : String = "AccessToken_Expired"
    
}
