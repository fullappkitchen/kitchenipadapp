//
//  ApiOperation.swift
//  Pods
//
//  Created by Viren Bhandari on 09/06/2017.
//
//

import Foundation
import Alamofire
import Alamofire_Gloss
import Gloss

public enum MappingType {
    case Object, Array, RawData
}

public typealias APICallback = (_: AnyObject?, _: (Int, String)?) -> Void

//TODO: simpler callbacks with progress percent
typealias UploadProgressCallback = (_ bytesWritten: UInt, _ totalBytesWritten: Int64, _ totalBytesExpectedToWrite: Int64) -> Void

typealias DownloadProgressCallback = (_ bytesRead: UInt, _ totalBytesRead: Int64, _ totalBytesExpectedToRead: Int64) -> Void

public protocol IApiOperation {
    func executeRequestWithCallback(callback: @escaping APICallback) -> Void
    func cancelRequest() -> Void
}

open class ApiOperation<T: Decodable> : IApiOperation {
        
    open var contentType: String?
    open var requestParameters: Dictionary<String, Any>? //these are body parameters if request is of type POST and same thing is appended to URL if request type is GET
    
    //TODO remove optional access from HTTPMethod and request url since these are mandatory fields
    open  var methodType: Alamofire.HTTPMethod?//Current request HTTP method type
    open var requestUrl: String? //Request URL
    open var mapping: MappingType? = .Object //Mapping type for this request
    
    open var type: T.Type
    open var httpBody: String?
    open var encoding: ParameterEncoding
    
    private var request: Request? //Current request in execution
    
    required public init(relativeUrl: String, methodType: Alamofire.HTTPMethod, _ type: T.Type,encoding:ParameterEncoding) {
        self.requestUrl = APIHelper.BaseURL + relativeUrl
        self.methodType = methodType
        self.type = type
        self.encoding = encoding
    }
    
    //TODO to write Error encapsulating entity for (Int, String)? in response callback
    public func executeRequestWithCallback(callback: @escaping (AnyObject?, (Int, String)?) -> Void) {
        
        let requestHeaders = self.getRequestHeaders()
        
        //TODO remove coupling of Alamofire using Specifications for RequestExecutor.
        let dataReq : DataRequest = Alamofire.request(self.requestUrl!, method: self.methodType!, parameters: self.requestParameters, encoding: encoding, headers: requestHeaders)
        
        if self.methodType == HTTPMethod.put {
            if httpBody != nil {
                dataReq.request?.httpBody
            }
        }
        
        dataReq.validate()
        
        switch self.mapping! {
        case .Array:
            dataReq.responseArray(type) { (response) in
                switch response.result {
                case .success(let rep):
                    callback(rep as AnyObject?, .none)
                    break
                case .failure(let error):
                    let responseObje : Any?
                    do{
                        responseObje = try JSONSerialization.jsonObject(with: response.data! as Data, options: JSONSerialization.ReadingOptions.mutableContainers)
                        let errorObj : ApiErrorHandler = ApiErrorHandler(json: responseObje as! JSON)!
//                        if(errorObj.exceptionType == ApiExceptionType.InvalidToken || errorObj.exceptionType == ApiExceptionType.ExpiredToken){
//                            self.sendNotification()
//                        }else{
                            callback(errorObj, ((response.result.error?._code)!, (response.result.error?.localizedDescription)!))
//                        }
                    }
                    catch {
                        guard case let .failure(error) = response.result else { return }
                        if let error = error as? AFError {
                            let errorObj = ApiErrorHandler(errorMessage: error.localizedDescription, errorCode: error.responseCode!)
                            callback(errorObj, (error.responseCode!, error.localizedDescription))
                        } else if let error = error as? URLError {
                            let errorObj = ApiErrorHandler(errorMessage: error.localizedDescription, errorCode: error.code.rawValue)
                            callback(errorObj,(error.code.rawValue, error.localizedDescription))
                        } else {
                            let errorObj = ApiErrorHandler(errorMessage: error.localizedDescription, errorCode: error._code)
                            callback(errorObj,(error._code, error.localizedDescription))
                        }
                    }
                    break
                }
            }
            break
        case .Object:
            dataReq.responseObject(type) { (response) in
                switch response.result {
                case .success(let rep):
                    callback(rep as AnyObject?, .none)
                    break
                case .failure(let error):
                    let responseObje : Any?
                    do{
                        responseObje = try JSONSerialization.jsonObject(with: response.data! as Data, options: JSONSerialization.ReadingOptions.mutableContainers)
                        let errorObj : ApiErrorHandler = ApiErrorHandler(json: responseObje as! JSON)!
                        if(errorObj.exceptionType == ApiExceptionType.InvalidToken || errorObj.exceptionType == ApiExceptionType.ExpiredToken){
                            self.sendNotification()
                        }else{
                            callback(errorObj, ((response.result.error?._code)!, (response.result.error?.localizedDescription)!))
                        }
                    }catch{
                        guard case let .failure(error) = response.result else { return }
                        if let error = error as? AFError {
                            let errorObj = ApiErrorHandler(errorMessage: error.localizedDescription, errorCode: error.responseCode!)
                            callback(errorObj, (error.responseCode!, error.localizedDescription))
                        } else if let error = error as? URLError {
                            let errorObj = ApiErrorHandler(errorMessage: error.localizedDescription, errorCode: error.code.rawValue)
                            callback(errorObj,(error.code.rawValue, error.localizedDescription))
                        } else {
                            let errorObj = ApiErrorHandler(errorMessage: error.localizedDescription, errorCode: error._code)
                            callback(errorObj,(error._code, error.localizedDescription))
                        }
                    }
                    break
                }
            }
            break
        case .RawData:
            break
        }
    }
    
    public func cancelRequest() {
        self.request!.cancel()
    }
    
    private func getRequestHeaders() -> Dictionary<String,String> {
        var headers = Dictionary<String, String>()
        headers[APIConstants.HeaderAccept] = APIConstants.MimeTypeJSON
        headers[APIConstants.HeaderAuthorization] = APIHelper.AuthHeader
        
        if let contentType = self.contentType {
            headers[APIConstants.HeaderContentType] = contentType
        }
        return headers
    }
    
    public func sendNotification(){
        let nc = NotificationCenter.default
        nc.post(name:Notification.Name(rawValue:APIConstants.ACCESS_TOKEN_EXPIRED),
                object: nil,
                userInfo: nil)
    }
    
}
