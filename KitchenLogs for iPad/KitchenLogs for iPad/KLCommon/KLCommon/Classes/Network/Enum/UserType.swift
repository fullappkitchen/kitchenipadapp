//
//  APIConstants.swift
//  Pods
//
//  Created by Viren Bhandari on 09/06/2017.
//
//

import Foundation

public enum UserType : String {
    case ADMIN
    case MANAGER
    case EMPLOY
}
