//
//  AuthService.swift
//  KitchenLogs
//
//  Created by Viren Bhandari on 9/8/17.
//  Copyright © 2017 KitchenLogs. All rights reserved.
//

import UIKit
import Alamofire

open class AuthService: NSObject {
    
    fileprivate static let loginUrl : String = "auth/login"
    fileprivate static let forgotPasswordUrl : String = "api/forgot-password"
    
    public static func loginUser(post: LoginPost, callback:@escaping APICallback) -> IApiOperation {
        let apiOperation = ApiOperation<LoginResponse>(relativeUrl: AuthService.loginUrl, methodType: Alamofire.HTTPMethod.post, LoginResponse.self, encoding:URLEncoding.default)
        apiOperation.contentType = APIConstants.MimeTypeJSON
        apiOperation.mapping = MappingType.Object
        apiOperation.requestParameters = post.toJSON()
        apiOperation.executeRequestWithCallback(callback: callback)
        return apiOperation
    }

    public static func forgotPassword(post : ForgotPasswordPost, callback:@escaping APICallback) -> IApiOperation {
        let apiOperation = ApiOperation<LoginResponse>(relativeUrl: AuthService.forgotPasswordUrl, methodType: Alamofire.HTTPMethod.post, LoginResponse.self, encoding:URLEncoding.default)
        apiOperation.contentType = APIConstants.MimeTypeJSON
        apiOperation.mapping = MappingType.Object
        apiOperation.requestParameters = post.toJSON()
        apiOperation.executeRequestWithCallback(callback: callback)
        return apiOperation
    }

}
