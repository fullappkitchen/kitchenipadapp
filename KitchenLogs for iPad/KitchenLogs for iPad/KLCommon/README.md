# KLCommon

[![CI Status](http://img.shields.io/travis/KLCommon.svg?style=flat)](https://travis-ci.org/Viren Bhandari/KLCommon)
[![Version](https://img.shields.io/cocoapods/v/KLCommon.svg?style=flat)](http://cocoapods.org/pods/KLCommon)
[![License](https://img.shields.io/cocoapods/l/KLCommon.svg?style=flat)](http://cocoapods.org/pods/KLCommon)
[![Platform](https://img.shields.io/cocoapods/p/KLCommon.svg?style=flat)](http://cocoapods.org/pods/KLCommon)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

KLCommon is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "KLCommon"
```
## Author

Vire R Bhandari, virenrb09@gmail.com

## License

KLCommon is available under the MIT license. See the LICENSE file for more info.
