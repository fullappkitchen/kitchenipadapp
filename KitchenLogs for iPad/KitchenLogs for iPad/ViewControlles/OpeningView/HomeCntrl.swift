//
//  OpeningCntrl.swift
//  KitchenLogs for iPad
//
//  Created by Manish on 9/9/17.
//  Copyright © 2017 KitchenLogs Ltd. All rights reserved.
//

import Foundation

class HomeCntrl: BaseViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet var tblHome:UITableView?
    
    override func viewDidLoad() {
        super.viewDidLoad();
        tblHome?.register(UINib(nibName: "TopHeader", bundle: nil), forCellReuseIdentifier: "TopHeader");
        tblHome?.register(UINib(nibName: "GridView", bundle: nil), forCellReuseIdentifier: "GridView");

        tblHome?.register(UINib(nibName: "MarkAllView", bundle: nil), forCellReuseIdentifier: "MarkAllView");

        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3;
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "cell";
        var cell:UITableViewCell?
        if indexPath.row == 0 {
            cell = tableView.dequeueReusableCell(withIdentifier: "TopHeader");
            if cell == nil {
                cell = UITableViewCell(style: .default, reuseIdentifier: "TopHeader");
            }
        }
        if indexPath.row == 1 {
            cell = tableView.dequeueReusableCell(withIdentifier: "GridView");
            if cell == nil {
                cell = UITableViewCell(style: .default, reuseIdentifier: "GridView");
            }
        }
        if indexPath.row == 2 {
            cell = tableView.dequeueReusableCell(withIdentifier: "MarkAllView");
            if cell == nil {
                cell = UITableViewCell(style: .default, reuseIdentifier: "MarkAllView");
            }
        }
        
       
        
        return cell!
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 200;

        }
        if indexPath.row == 1 {
            return 116;
            
        }
        if indexPath.row == 2 {
            return 300;
            
        }
        return 0;
    }
    
    @IBAction func clickedToMenu(_ sender: Any) {
        getAppDelegate().sliderView.openLeft();
    }
}
