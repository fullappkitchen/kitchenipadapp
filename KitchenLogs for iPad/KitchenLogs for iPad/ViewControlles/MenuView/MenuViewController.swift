//
//  MenuViewController.swift
//  KitchenLogs for iPad
//
//  Created by Manish on 10/09/17.
//  Copyright © 2017 KitchenLogs Ltd. All rights reserved.
//

import UIKit

class MenuViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource,LinkLabelGastureDelegate {

    @IBOutlet weak var lblPrivacy: LinkAttributatedLabel!
    @IBOutlet weak var tblMenu: UITableView!
    @IBOutlet weak var lblWelcomeUser: UILabel!
    var sectionArray = [String]()
    var menuArray = [String]()
    var sectionImageArray = [String]()
    var isAddcheckOpen:Bool!
    override func viewDidLoad() {
        super.viewDidLoad()
        attributedString();
        isAddcheckOpen = false;
        self.view.backgroundColor = UIColor.getGreenColor();
    sectionArray = ["Oprning Check","Cleaning Check","Delivery Check","Closing Check","Add Check","View Log"]
    menuArray = ["Food","Appliances","Pest Control","Injury Log","Probe Check"]
    sectionImageArray = ["icons8FullMoon100",
                           "icons8FullMoon100-1",
                           "icons8FullMoon100-2",
                           "icons8FullMoon100-3",
                           "icons8FullMoon100-4","icons8FullMoon100-4"]
    self.tblMenu.register(UINib(nibName: "MenuCustomCell", bundle: nil), forCellReuseIdentifier: "MenuCustomCell")
    self.tblMenu.backgroundColor = UIColor.getGreenColor();
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
  // UITable View numberOfSections Delegate
    func numberOfSections(in tableView: UITableView) -> Int {
            return sectionArray.count;
    }
    
    // UITable View viewForHeaderInSection Delegate
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
            let headerSubView = UIView()
            headerView.frame = CGRect(x: 0 , y: 0, width: self.view.frame.size.width, height: 60)
            headerSubView.frame = CGRect(x: 0 , y: 0, width: self.view.frame.size.width, height: 60)
            headerSubView.backgroundColor = UIColor.getGreenColor();
            headerView.backgroundColor = UIColor.getGreenColor();
            // Create UILabel
            let addLabel:UILabel = UILabel()
            addLabel.frame = CGRect(x: 140, y: 10, width: 200, height: 50)
            addLabel.textAlignment = .left
            addLabel.text = sectionArray[section];
            addLabel.numberOfLines=1
            addLabel.textColor=UIColor.white;
            addLabel.font=UIFont.systemFont(ofSize: 20)
            addLabel.backgroundColor=UIColor.clear
            headerSubView.addSubview(addLabel)
            // Create UIbutton
            let button:UIButton = UIButton(frame: headerView.frame)
            button.backgroundColor = UIColor.clear;
            button.setTitle("", for: .normal)
            button.addTarget(self, action:#selector(self.buttonClicked(sender:)), for: .touchUpInside)
            button.tag = section;
            headerSubView.addSubview(button)
            headerView.addSubview(headerSubView);
            
            let image = UIImage(named:sectionImageArray[section]) // is 121x121
            let imageView = UIImageView(frame: CGRect(x: 80,y: 22, width: 25 , height: 25))
            imageView.image = image
            imageView.contentMode = .scaleAspectFit
            headerView.addSubview(imageView)
        return headerView
    }
    
    
    // UITable View heightForRowAt Delegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0;
    }
    
    // UITable View numberOfRowsInSection Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 4 {
        return (isAddcheckOpen == true) ? 5 : 0;
        } else {
            return 0;
        }
    }
    
    // UITable View cellForRowAt Delegate
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell:MenuCustomCell?
        let cellIdentifier = "MenuCustomCell"
        cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? MenuCustomCell
        if cell == nil {
            cell = MenuCustomCell(style: UITableViewCellStyle.value1, reuseIdentifier: cellIdentifier)
        }
        cell?.lblTitle.text = menuArray[indexPath.row];
        return cell!
 
    }
    
    // UITable View didSelectRowAt Delegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
    // Section Header Button Clicked for Navigate to specific Screen
    func buttonClicked(sender:UIButton){
        if sender.tag == 4 {
        isAddcheckOpen = (isAddcheckOpen == false) ? true :false;
        let sectionIndex = IndexSet(integer: 4)
        tblMenu.reloadSections(sectionIndex, with: .automatic)
        tblMenu.reloadData();
        } else {
        isAddcheckOpen = false;
        tblMenu.reloadData();
            if sender.tag == 0 {
                
            } else if sender.tag == 1 {
                
            }else if sender.tag == 2 {
                
            }else if sender.tag == 3 {
                
            }
            else if sender.tag == 5 {
            navigateWithMenu(toViewController: "ViewLogController",valAnyObj:"" as AnyObject);
            }
            
        
        
        }
    }

    // Make URL Link on Label with Attribute
    func attributedString(){
        // Terms and Condition Label Code
        let underlineAttributedString = NSMutableAttributedString(string: lblPrivacy.text!);
        underlineAttributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor.blue, range:NSRange(location:0,length:14))
        underlineAttributedString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range:NSRange(location:0,length:14))
        underlineAttributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor.blue, range:NSRange(location:17,length:20))
        underlineAttributedString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range:NSRange(location:17,length:20))
        lblPrivacy.attributedText = underlineAttributedString;
        lblPrivacy.delegate = self
    }
    
    @IBAction func clickedToLogout(_ sender: Any) {
    }
    
    // Attributed Label Delegate
    func attributedLabelCallBack(strVal:String){
        if strVal == "TERMS & CONDITIONS" {
            Helper.OpenSafariWithURl("https://www.kitchenlo.gs/terms/");
            
        }
        else if strVal == "PRIVACY POLICY" {
            Helper.OpenSafariWithURl("https://www.kitchenlo.gs/privacy-policy/");
            
        }
    }

    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
