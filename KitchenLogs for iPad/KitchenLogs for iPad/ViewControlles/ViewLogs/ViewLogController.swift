//
//  ViewLogController.swift
//  KitchenLogs for iPad
//
//  Created by Manish on 10/09/17.
//  Copyright © 2017 KitchenLogs Ltd. All rights reserved.
//

import UIKit

class ViewLogController: BaseViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tblViewLog: UITableView!
    @IBOutlet weak var vwTopView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblViewLog.register(UINib(nibName: "ViewLogCell", bundle: nil), forCellReuseIdentifier: "ViewLogCell")
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell:ViewLogCell?
        let cellIdentifier = "ViewLogCell"
        cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? ViewLogCell
        if cell == nil {
            cell = ViewLogCell(style: UITableViewCellStyle.value1, reuseIdentifier: cellIdentifier)
        }
        
        return cell!
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
