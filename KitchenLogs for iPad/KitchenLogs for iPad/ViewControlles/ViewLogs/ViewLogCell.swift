//
//  ViewLogCell.swift
//  KitchenLogs for iPad
//
//  Created by rajdeep on 10/09/17.
//  Copyright © 2017 KitchenLogs Ltd. All rights reserved.
//

import UIKit

class ViewLogCell: UITableViewCell {
    @IBOutlet weak var namelabel: UILabel?
    @IBOutlet weak var templateLabel: UILabel?
    @IBOutlet weak var timeLabel: UILabel?
    @IBOutlet weak var datelabel: UILabel?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
