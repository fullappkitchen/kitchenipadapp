//
//  AddressUserPreference.swift
//  EasyShop24
//
//  Created by Manish on 6/4/15.
//  Copyright (c) 2015 Nzt. All rights reserved.
//

import UIKit

class AddressUserPreference: NSObject, NSCoding {

    var name:String!;
    var phoneNumber:String!;
    var houseNumber:String!;
    var subArea:String!;
    var areaCity:String!;
    var userCode:String!;
    var userCodeAddress:String!;
    
    // Init Method For Address
    init(nme:String, phNo:String, houseNo:String, subLocality:String,city:String, usercode:String, usercodeAdrs:String) {
        name = nme;
        phoneNumber = phNo;
        houseNumber = houseNo;
        subArea = subLocality;
        areaCity = city;
        userCode = usercode;
        userCodeAddress = usercodeAdrs;
    }
    
    
    required init?(coder decoder: NSCoder) {
        self.name = decoder.decodeObject(forKey: "name") as! String?
        self.phoneNumber = decoder.decodeObject(forKey: "phoneNumber") as! String?
        self.houseNumber = decoder.decodeObject(forKey: "houseNumber")as! String?
        self.subArea = decoder.decodeObject(forKey: "subArea") as! String?
        self.areaCity = decoder.decodeObject(forKey: "areaCity") as! String?
        self.userCode = decoder.decodeObject(forKey: "usercode") as! String?
        self.userCodeAddress = decoder.decodeObject(forKey: "usercodeaddress") as! String?
        
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(self.name, forKey: "name");
        coder.encode(self.phoneNumber, forKey: "phoneNumber");
        coder.encode(self.houseNumber, forKey: "houseNumber");
        coder.encode(self.subArea, forKey: "subArea");
        coder.encode(self.areaCity, forKey: "areaCity");
        coder.encode(self.userCode, forKey: "usercode");
        coder.encode(self.userCodeAddress, forKey: "usercodeaddress");
      
        
    }
    
    
}
