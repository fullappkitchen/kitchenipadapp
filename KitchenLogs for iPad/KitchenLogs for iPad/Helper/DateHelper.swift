//
//  DateHelper.swift
//  GoParties
//
//  Created by Manish  on 04/01/16.
//  Copyright © 2016 Shubham . All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class DateHelper: NSObject {
    
    /*
    NSDate *yourDate = [NSDate date];//now in this example
    NSTimeInterval epochTimestamp = [yourDate timeIntervalSince1970];
    NSString *epochTimestampString = [@(epochTimestamp) stringValue];
    NSLog(@"%@",epochTimestampString);
    
    
    */
    
    class func convertStringDateToDate(_ strDate:String)->String {
        let dateFormatter = DateFormatter();
        dateFormatter.timeZone = TimeZone.autoupdatingCurrent;
        dateFormatter.date(from: "yyyy-MM-dd HH:mm:ss");
        let date = dateFormatter.date(from: strDate);
        
        dateFormatter.date(from: "yyyy-MM-dd");
        let stringDate = dateFormatter.string(from: date!);
        return stringDate;
    }
    
    // DealFormate
    class func convertTimeStampToStringDealDate(_ strTimeStamp:String)->String
    {
        
        let dateFormatter = DateFormatter();
        let _interval = Double(strTimeStamp);
        let date = Date.init(timeIntervalSince1970: _interval!);
        dateFormatter.dateFormat = "MM-dd-yyyy EEE h:mm a";
        let finalDate = dateFormatter.string(from: date);
        return finalDate;
        
    }
    
    // 03 Dec Format
    class func convertTimeStampToStringDayMonth(_ strTimeStamp:String)->String
    {
        
        let dateFormatter = DateFormatter();
        let _interval = Double(strTimeStamp);
        let date = Date.init(timeIntervalSince1970: _interval!);
        dateFormatter.dateFormat = "dd MMM";
        let finalDate = dateFormatter.string(from: date);
        return finalDate;
        
    }

    
    
    class func convertTimeStampToDate(_ strTimeStamp:String!)->Date
    {
        
        let dateFormatter = DateFormatter();
        let _interval = Double(strTimeStamp);
        let date = Date.init(timeIntervalSince1970: _interval!);
        dateFormatter.dateFormat = "MM/dd/yyyy";
        //let finalDate = dateFormatter.stringFromDate(date);
        return date;
        
    }
    
    
    class func timeStampToDate(_ strTimeStamp:String!)->Date
    {
        
        let dateFormatter = DateFormatter();
        let _interval = Double(strTimeStamp);
        let date = Date.init(timeIntervalSince1970: _interval!);
        dateFormatter.dateFormat = "yyyy-MM-dd";
        //let finalDate = dateFormatter.stringFromDate(date);
        return date;
        
    }
    class func timeStampToDate1(_ strTimeStamp:String!)->Date
    {
        
        let dateFormatter = DateFormatter();
        let _interval = Double(strTimeStamp);
        let date = Date.init(timeIntervalSince1970: _interval!);
        dateFormatter.dateFormat = "yyyy-MM-dd h:mm a";
        //let finalDate = dateFormatter.stringFromDate(date);
        return date;
        
    }

    
    

    
    
    class func convertTimeStampToString(_ strTimeStamp:String)->String
    {
        
        let dateFormatter = DateFormatter();
        let _interval = Double(strTimeStamp);
        let date = Date.init(timeIntervalSince1970: _interval!);
        dateFormatter.dateFormat = "dd/MM/yyyy";
        let finalDate = dateFormatter.string(from: date);
        return finalDate;
        
    }
    
    
    class func convertTimeStampToDateTimeString(_ strTimeStamp:String)->String
    {
        let dateFormatter = DateFormatter();
        let _interval = Double(strTimeStamp);
        let date = Date.init(timeIntervalSince1970: _interval!);
        dateFormatter.dateFormat = "d MMMM EEEE , h:mm a" //"MM/dd/yyyy";
        let finalDate = dateFormatter.string(from: date);
        return finalDate;
        
    }
    
    class func getDateFromTimeStamp(_ strTimeStamp:String)-> Date{
        let _interval = Double(strTimeStamp);
        let date = Date.init(timeIntervalSince1970: _interval!);
        return date;
    }
    
    
    class func convertTimeStampToDateTimeString1(_ strTimeStamp:String)->String
    {
        let dateFormatter = DateFormatter();
        let _interval = Double(strTimeStamp);
        let date = Date.init(timeIntervalSince1970: _interval!);
        dateFormatter.dateFormat = "d MMMM, h:mm a" //"MM/dd/yyyy";
        let finalDate = dateFormatter.string(from: date);
        return finalDate;
        
    }
    
    
    
    class func convertTimeStampToDateStr(_ strTimeStamp:String)->String
    {
        let dateFormatter = DateFormatter();
        let _interval = Double(strTimeStamp);
        let date = Date.init(timeIntervalSince1970: _interval!);
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm" //"MM/dd/yyyy";
        let finalDate = dateFormatter.string(from: date);
        return finalDate;
        
    }
    
    class func convertTimeStampToDateStrAmPm(_ strTimeStamp:String)->String
    {
        let dateFormatter = DateFormatter();
        let _interval = Double(strTimeStamp);
        let date = Date.init(timeIntervalSince1970: _interval!);
        dateFormatter.dateFormat = "dd-MM-yyyy h:mm a" //"MM/dd/yyyy";
        let finalDate = dateFormatter.string(from: date);
        return finalDate;
        
    }
    

    
    class func convertTimeStampToDateStr1(_ dateStr:String)->String
    {
        let dateFormatter = DateFormatter()
        //dateFormatter.dateFormat = "MM/dd/yyyy"
        dateFormatter.dateFormat = "dd-MM-yyyy hh:mm a"
        let date = dateFormatter.date(from: dateStr)
        let intervalTimeStamp = date!.timeIntervalSince1970;
        let strTimeStamp = String(intervalTimeStamp);
        return strTimeStamp;
        
    }

    

    
    class func convertTimeStampToTime(_ strTimeStamp:String)->String
    {
        let dateFormatter = DateFormatter();
        let _interval = Double(strTimeStamp);
        let date = Date.init(timeIntervalSince1970: _interval!);
        dateFormatter.dateFormat = "HH:mm" //"HH:mm";
        let finalDate = dateFormatter.string(from: date);
        return finalDate;
        
    }
    
    class func convertTimeStampToTimeWithAM(_ strTimeStamp:String)->String
    {
        let dateFormatter = DateFormatter();
        let _interval = Double(strTimeStamp);
        let date = Date.init(timeIntervalSince1970: _interval!);
        dateFormatter.dateFormat = "h:mm a" //"HH:mm";
        let finalDate = dateFormatter.string(from: date);
        return finalDate;
        
    }

    
    
    class func convertTimetoTimeString(_ strTimeStamp:String)->String
    {
        let dateFormatter = DateFormatter();
        dateFormatter.dateFormat = "d MMMM EEEE , h:mm a" //"MM/dd/yyyy";
        let finalDate = dateFormatter.string(from: Date());
        return finalDate;
        
    }
    
    
    class func convertToDateStr(_ strTimeStamp:String)->String
    {
        
        let dateFormatter = DateFormatter();
        let _interval = Double(strTimeStamp);
        let date = Date.init(timeIntervalSince1970: _interval!);
        dateFormatter.dateFormat = "dd-MM-yyyy";
        let finalDate = dateFormatter.string(from: date);
        return finalDate;
        
    }
    
    
    
    
    
    class func convertDateToTimeStamp(_ date:Date)->String
    {
        let intervalTimeStamp = date.timeIntervalSince1970;
        let strTimeStamp = String(intervalTimeStamp);
        let dateArray = strTimeStamp.components(separatedBy: ".");
        return dateArray[0];
        
    }
    //EEEE-dd-MMM
    class func convertDatesStrToTimeStamp(_ dateStr:String)->String
    {
        let dateFormatter = DateFormatter()
        //dateFormatter.dateFormat = "MM/dd/yyyy"
        dateFormatter.dateFormat = "MMM dd, yyyy"
        let date = dateFormatter.date(from: dateStr)
        let intervalTimeStamp = date!.timeIntervalSince1970;
        let strTimeStamp = String(intervalTimeStamp);
        return strTimeStamp;
        
    }
    
    //MM/dd/yyyy
    class func convertDateStrToTimeStamp(_ dateStr:String)->String
    {
        let dateFormatter = DateFormatter()
        //dateFormatter.dateFormat = "EEEE-dd-MMM"
         dateFormatter.dateFormat = "MM/dd/yyyy"
        let date = dateFormatter.date(from: dateStr)
        let intervalTimeStamp = date!.timeIntervalSince1970;
        let strTimeStamp = String(intervalTimeStamp);
        return strTimeStamp;
        
    }
    
    class func convertDateValToTimeStamp(_ dateStr:String)->String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let date = dateFormatter.date(from: dateStr)
        let intervalTimeStamp = date!.timeIntervalSince1970;
        let strTimeStamp = String(intervalTimeStamp);
        return strTimeStamp;
        
    }
    //07-May-2017 Format
    class func convertDateStrToTimeStampMonth(_ dateStr:String)->String
    {
        let dateFormatter = DateFormatter();
        let _interval = Double(dateStr);
        let date = Date.init(timeIntervalSince1970: _interval!);
        dateFormatter.dateFormat = "dd-MMM-yyyy" //"HH:mm";
        let finalDate = dateFormatter.string(from: date);
        return finalDate;
    }
    
    
    class func convertDateStrToTimeStamp1(_ dateStr:String)->String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
        let date = dateFormatter.date(from: dateStr)
        let intervalTimeStamp = date!.timeIntervalSince1970;
        let strTimeStamp = String(intervalTimeStamp);
        return strTimeStamp;
        
    }
    
    class func convertDateStrToTimeStampFilter(_ dateStr:String)->String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEE, dd-MM-yyyy"
        let date = dateFormatter.date(from: dateStr)
        let intervalTimeStamp = date!.timeIntervalSince1970;
        let strTimeStamp = String(intervalTimeStamp);
        let dateArray = strTimeStamp.components(separatedBy: ".");
        return dateArray[0];
    }
    
    class func convertDateStrToDateFilter(_ dateStr:String)->Date
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEE, dd-MM-yyyy"
        let date = dateFormatter.date(from: dateStr)
        return date!;
    }
    
    class func convertDateStrToDateFilterDate(_ dateStr:String)->Date
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy"
        dateFormatter.timeZone = TimeZone(identifier:"UTC")
        let date = dateFormatter.date(from: dateStr)
        return date!;
    }
    
    
    
    class func convertDateStrToTimeStamp2(_ dateStr:String)->String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        let date = dateFormatter.date(from: dateStr)
        let intervalTimeStamp = date!.timeIntervalSince1970;
        let strTimeStamp = String(intervalTimeStamp);
        return strTimeStamp;
        
    }
    
    
    class func convertDateStrToTimeStampnum(_ dateStr:String)->String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        let date = dateFormatter.date(from: dateStr)
        let intervalTimeStamp = date!.timeIntervalSince1970;
        let strTimeStamp = String(intervalTimeStamp);
        return strTimeStamp;
        
    }
    
    class func convertDateStrToTimeParty(_ dateStr:String)->String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
        let date = dateFormatter.date(from: dateStr.lowercased())
        let intervalTimeStamp = date!.timeIntervalSince1970;
        let strTimeStamp = String(intervalTimeStamp);
        return strTimeStamp;
        
    }
    
    
    class func convertStringDateTostrdate(_ strDate:String)->String {
        let dateFormatter = DateFormatter();
        //dateFormatter.timeZone = NSTimeZone.localTimeZone();
        // dateFormatter.dateFromString("dd-MM-yyyy HH:mm");
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm";
        dateFormatter.timeZone = TimeZone(identifier:"UTC")
        let date = dateFormatter.date(from: strDate);
        dateFormatter.dateFormat = "MMM dd, yyyy";
        
        // dateFormatter.dateFromString("MMMM dd, yyyy");
        let stringDate = dateFormatter.string(from: date!);
        return stringDate;
    }
    
    class func convertStringDateTostrdate1(_ strDate:String)->String {
        let dateFormatter = DateFormatter();
        //dateFormatter.timeZone = NSTimeZone.localTimeZone();
        // dateFormatter.dateFromString("dd-MM-yyyy HH:mm");
        dateFormatter.dateFormat = "EEEE-dd-MMM";
        dateFormatter.timeZone = TimeZone(identifier:"UTC")
        let date = dateFormatter.date(from: strDate);
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm";
        
        // dateFormatter.dateFromString("MMMM dd, yyyy");
        let stringDate = dateFormatter.string(from: date!);
        return stringDate;
    }
    
    class func convertStringDateTostrdate2(_ strDate:String)->Date {
        let dateFormatter = DateFormatter();
        //dateFormatter.timeZone = NSTimeZone.localTimeZone();
        // dateFormatter.dateFromString("dd-MM-yyyy HH:mm");
        dateFormatter.dateFormat = "dd/MM/yyyy";
        dateFormatter.timeZone = TimeZone(identifier:"UTC")
        let date = dateFormatter.date(from: strDate);
        dateFormatter.dateFormat = "yyyy-MM-dd";
        
        // dateFormatter.dateFromString("MMMM dd, yyyy");
        //let stringDate = dateFormatter.stringFromDate(date!);
        return date!;
    }

    
    
    class func convertTimeStampToStr1(_ strTimeStamp:String)->String
    {
        let dateFormatter = DateFormatter();
        let _interval = Double(strTimeStamp);
        let date = Date.init(timeIntervalSince1970: _interval!);
        dateFormatter.dateFormat = "MMM dd, yyyy";
        let finalDate = dateFormatter.string(from: date);
        return finalDate;
    }
    
    class func convertTimeStampToCalendarDate(_ strTimeStamp:String)->String
    {
        let dateFormatter = DateFormatter();
        let _interval = Double(strTimeStamp);
        let date = Date.init(timeIntervalSince1970: _interval!);
        dateFormatter.dateFormat = "MMM dd";
        let finalDate = dateFormatter.string(from: date);
        return finalDate;
    }
    
    
    class func convertTimeStampToDateTime(_ strTimeStamp:String)->String
    {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss ZZZ"// "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        // formatter.timeZone = NSTimeZone(forSecondsFromGMT: 0)
        //formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
        // formatter.timeZone = NSTimeZone(abbreviation: "UTC");
        //print(formatter.stringFromDate(date))
        
        
        //formatter.timeZone = NSTimeZone(abbreviation: "UTC");
        let utcTimeZoneStr = formatter.string(from: date);
        
        
        
        return utcTimeZoneStr;
    }
    
    class func convertTimeStampToStr(_ strTimeStamp:String)->String
    {
        let dateFormatter = DateFormatter();
        let _interval = Double(strTimeStamp);
        let date = Date.init(timeIntervalSince1970: _interval!);
        dateFormatter.dateFormat = "MMMM,dd h:mm a" //"MM/dd/yyyy";
        let finalDate = dateFormatter.string(from: date);
        return finalDate;
    }
    
    
    class func convertStrToTimeStamp(_ dateStr:String)->String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let date = dateFormatter.date(from: dateStr)
        print("Date is:\(String(describing: date))");
        let intervalTimeStamp = date!.timeIntervalSince1970;
        let strTimeStamp = String(intervalTimeStamp);
        return strTimeStamp;
        
    }
    // For Party Spot Management
    class func convertStrToTimeStampParty(_ dateStr:String)->TimeInterval
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-dd-MMM"
        let date = dateFormatter.date(from: dateStr)
        print("Date is:\(String(describing: date))");
        let intervalTimeStamp = date!.timeIntervalSince1970;
        return intervalTimeStamp * 1000;
        
    }
    
    class func convertStrToTimeStampFilter(_ dateStr:String)->TimeInterval
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
        //dateFormatter.timeZone = NSTimeZone.defaultTimeZone();
        let date = dateFormatter.date(from: dateStr)
        
        print("Date is:\(String(describing: date))");
        let intervalTimeStamp = date!.timeIntervalSince1970;
        return intervalTimeStamp * 1000;
        
    }

    // For Party Spot Management
    class func convertStrToStrFormat(_ dateStr:String)->NSString
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-dd-MMM"
        let date = dateFormatter.date(from: dateStr)
        dateFormatter.dateFormat = "hh:mm a, dd MMM"
        let dateStr = dateFormatter.string(from: date!)
        return dateStr as NSString;
        
    }

    
    
    
    
    // to get the no of days between two dates
    class func daysBetweenDate(_ startDate: Date, endDate: Date) -> Int
    {
        let calendar = Calendar.current
        let components = (calendar as NSCalendar).components([.day], from: startDate, to: endDate, options: [])
        return components.day!
    }
    
    // to get the no of hours between a day
    class func hoursBetweenDate(_ startDate: Date, endDate: Date) -> Int{
        let calendar = Calendar.current
        let components = (calendar as NSCalendar).components([.hour], from: startDate, to: endDate, options: [])
        return components.hour!
    }
    
    // Next Month Date
    class func getNextMonth(_ selectedDate:Date)-> Date{
        let cal = NSCalendar.current
        return cal.date(byAdding: .month, value: 1, to: NSDate() as Date)!
    }
    // Previous Month Date
    class func getPreviousMonth(_ selectedDate:Date)->Date{
        let cal = NSCalendar.current
            return cal.date(byAdding: .month, value: -1, to: NSDate() as Date)!
    }
    
    
    
    // TKCalendar Date Format Here
    class  func getStringfromDate(_ date:Date)->String{
        let formatter = DateFormatter();
        formatter.dateFormat = "MM/dd/yyyy";
        return formatter.string(from: date);
    }
    
    
    class func getStrFromCurrentDate()->String{
        let currentDate = Date();
        let formatter = DateFormatter();
        formatter.dateFormat = "dd-MM-yyyy HH:mm";
        return formatter.string(from: currentDate);
    }
    
    class  func getDateformString(_ stringdate:String)->String{
        let formatter = DateFormatter();
        formatter.dateFormat = "MM/dd/yyyy";
        let date = formatter.date(from: stringdate);
        return formatter.string(from: date!);
    }
    
    class  func getDateformDate(_ stringdate:Date)->Date{
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss ZZZ"
        let str = formatter.string(from: stringdate);
        let dateArray = str.components(separatedBy: " ");
        let formatter2 = DateFormatter();
        formatter2.dateFormat = "yyyy-MM-dd";
        return formatter2.date(from: dateArray[0])!;
    }
    
    class  func getTimeformDate(_ stringdate:Date)->Date{
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss ZZZ"
        let str = formatter.string(from: stringdate);
        let dateArray = str.components(separatedBy: " ");
        let formatter2 = DateFormatter();
        formatter2.dateFormat = "HH:mm:ss";
        return formatter2.date(from: dateArray[1])!;
    }
    
    class  func getDateStr(_ stringdate:Date)->String{
        let formatter = DateFormatter()
        formatter.dateFormat = "MMMM yyyy"
        let str = formatter.string(from: stringdate);
        return str;
    }
    
    class  func getFilterDatetoStr(_ stringdate:Date)->String{
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM dd, yyyy"
        let str = formatter.string(from: stringdate);
        return str;
    }
    class  func getFilter2DatetoStr1(_ stringdate:Date)->String{
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM dd, yyyy"
        let str = formatter.string(from: stringdate);
        let dt = formatter.date(from: str);
        let strDt = formatter.string(from: dt!);
        return strDt;
    }

    class  func getFilterDatetoStr1(_ stringdate:Date)->String{
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss ZZZ"
        let str = formatter.string(from: stringdate);
        let dt = formatter.date(from: str);
        formatter.dateFormat = "EEEE-dd-MMM"
        let strDt = formatter.string(from: dt!);
        return strDt;
    }

    
    
    class func convertDateFromString(_ strTimeStamp:String)->Date
    {
        let dateFormatter = DateFormatter();
        dateFormatter.dateFormat = "dd-MM-yyyy";
        return dateFormatter.date(from: strTimeStamp)!;
        
    }
    
    class func convertTimeFromString(_ strTimeStamp:String)->Date
    {
        let dateFormatter = DateFormatter();
        dateFormatter.dateFormat = "h:mm a";
        return dateFormatter.date(from: strTimeStamp)!;
        
    }
    

    class func convertTimeFromStringfromStamp(_ strTimeStamp:String)->String
    {
        let dateFormatter = DateFormatter();
        let _interval = Double(strTimeStamp);
        let date = Date.init(timeIntervalSince1970: _interval!);
        dateFormatter.dateFormat = "h:mm a";
        let finalDate = dateFormatter.string(from: date);
        return finalDate;
        
    }

    
    // TKCalendar Date Format Here
    class  func getStringfromDateforChatting(_ date:Date)->String{
        let formatter = DateFormatter();
        formatter.dateFormat = "h:mm a";
        return formatter.string(from: date);
    }
    
    
    class func convertStringFromDate(_ date:Date)->String
    {
        let dateFormatter = DateFormatter();
        dateFormatter.dateFormat = "dd-MM-yyyy";
        return dateFormatter.string(from: date);
        
    }
    
    class func convertStrFromDate(_ date:Date)->String
    {
        let dateFormatter = DateFormatter();
        dateFormatter.dateFormat = "dd";
        return dateFormatter.string(from: date);
        
    }
    
    
    
    class func convertDateLocalFromUTC(_ utcDate:Date)->Date{
        /*
        NSString *dateStr = @"2012-07-16 07:33:01";
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
        [dateFormatter1 setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *date = [dateFormatter1 dateFromString:dateStr];
        NSLog(@"date : %@",date);
        
        NSTimeZone *currentTimeZone = [NSTimeZone localTimeZone]; // <- Local time zone
        NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
        
        NSInteger currentGMTOffset = [currentTimeZone secondsFromGMTForDate:date1];
        NSInteger gmtOffset = [utcTimeZone secondsFromGMTForDate:date1];
        NSTimeInterval gmtInterval = currentGMTOffset - gmtOffset;
        
        NSDate *destinationDate = [[[NSDate alloc] initWithTimeInterval:gmtInterval    sinceDate:date1] autorelease];
        
        NSDateFormatter *dateFormatters = [[NSDateFormatter alloc] init];
        [dateFormatters setDateFormat:@"dd-MMM-yyyy hh:mm"];
        [dateFormatters setDateStyle:NSDateFormatterShortStyle];
        [dateFormatters setTimeStyle:NSDateFormatterShortStyle];
        [dateFormatters setDoesRelativeDateFormatting:YES];
        [dateFormatters setTimeZone:[NSTimeZone systemTimeZone]];
        dateStr = [dateFormatters stringFromDate: destinationDate];
        NSLog(@"DateString : %@", dateStr);
        */
        
        print("Before Convert Date is:\(utcDate)")
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = "yyyy-MM-dd HH:mm:ss ZZZ";
        let dateStr = dateFormatter1.string(from: utcDate);
        let date = dateFormatter1.date(from: dateStr);
        
        let currentTimeZone = TimeZone.autoupdatingCurrent;
        let utcTimeZone = TimeZone(abbreviation: "UTC");
        
        let currentGMTOffset = currentTimeZone.secondsFromGMT(for: date!);
        let gmtOffset = utcTimeZone?.secondsFromGMT(for: date!);
        let gmtInterval = Double(currentGMTOffset - gmtOffset!);
        
        let destinationDate = Date(timeInterval: gmtInterval, since: date!);
        
        let dateFormatters = DateFormatter();
        dateFormatters.dateFormat = "yyyy-MM-dd HH:mm:ss ZZZ";
        let date_Str = dateFormatters.string(from: destinationDate);
        
        let dtFormate = DateFormatter();
        dtFormate.dateFormat = "yyyy-MM-dd HH:mm:ss ZZZ";
        let finalDate = dtFormate.date(from: date_Str);
        
        
        
        print("After Convert Date is:\(finalDate)")
        
        return finalDate!;
        
        
        
        
    }
    
    
    class func startDayTime(_ date:Date)->Date {
        let cal: Calendar = Calendar(identifier: Calendar.Identifier.indian)
        let startDay: Date = (cal as NSCalendar).date(bySettingHour: 0, minute: 0, second: 1, of: date, options: NSCalendar.Options())!
        return startDay;
    }
    
    class func endDayTime(_ date:Date)->Date {
        let cal: Calendar = Calendar(identifier: Calendar.Identifier.indian)
        let startDay: Date = (cal as NSCalendar).date(bySettingHour: 23, minute: 59, second: 59, of: date, options: NSCalendar.Options())!
        return startDay;
    }
    
    class func getDaysDifference(_ partyDate:String)->String{
//        let dateFormatter = NSDateFormatter()
//        dateFormatter.dateFormat = "MM/dd/yyyy"
//        //let startDate:NSDate = dateFormatter.dateFromString(start)!
//        let endDate:NSDate = dateFormatter.dateFromString(partyDate)!
//        let cal = NSCalendar.currentCalendar()
//        let unit:NSCalendarUnit = .Day
//        let components = cal.components(unit, fromDate: NSDate(), toDate: endDate, options: NSCalendarOptions())
//        print(components.day)
//        if components.day > 1 {
//            return String(components.day) + " DAYS LEFT";
//        } else if components.day == 0 {
//            return String(components.day) + " TODAY"
//        }else {
//            return "HAPPENED"
//        }
        
        
        if partyDate == "" {
            return "";
        }
        let timeStr = Double(partyDate)!/1000
        let endDate = DateHelper.convertTimeStampToDate(String(timeStr));
        let f:DateFormatter = DateFormatter()
        f.timeZone = TimeZone.autoupdatingCurrent
        f.dateFormat = "yyyy-M-dd'T'HH:mm:ss.SSSZZZ"
        
        let now = f.string(from: Date())
        //let startDate = f.dateFromString(endDate)
        let startDate = f.date(from: now)
        let calendar: Calendar = Calendar.current
        
        let calendarUnits:NSCalendar.Unit = [.weekOfMonth, .day, .hour, .minute, .second];
        let dateComponents = (calendar as NSCalendar).components(calendarUnits, from: startDate!, to: endDate, options: NSCalendar.Options())
        
        let weeks = Int(dateComponents.weekOfMonth!)
        let days = Int(dateComponents.day!)
        let hours = Int(dateComponents.hour!)
        let min = Int(dateComponents.minute!)
        // let sec = abs(dateComponents.second)
        
        var timeAgo = ""
        
        if min < 0 {
            timeAgo = "HAPPENED"
        }
        
        if (min > 0){
            if (min > 1) {
                timeAgo = "\(min) Minutes to go"
            } else {
                timeAgo = "\(min) Minute to go"
            }
        }
        
        if(hours > 0){
            if (hours > 1) {
                timeAgo = "\(hours) Hours to go"
            } else {
                timeAgo = "\(hours) Hour to go"
            }
        }
        
        if (days > 0) {
            if (days > 1) {
                timeAgo = "\(days) Days to go"
            } else {
                timeAgo = "\(days) Day to go"
            }
        }
        
        if(weeks > 0){
            if (weeks > 1) {
                timeAgo = "\(weeks) Weeks to go"
            } else {
                timeAgo = "\(weeks) Week to go"
            }
        }
        
        print("timeAgo is===> \(timeAgo)")
        return timeAgo;

        
    }
    
    /*
    class func getDays(dt:String)->String{
    if dt != "" {
    return "";
    }
    let timeStr = Double(dt)!/1000
    let endDate = DateHelper.convertTimeStampToDate(String(timeStr));
    let cal = NSCalendar.currentCalendar()
    let unit:NSCalendarUnit = .Day
    let components = cal.components(unit, fromDate: NSDate(), toDate: endDate, options: NSCalendarOptions())
    print(components.day)
    if components.day == 0 {
    return "Today"
    } else if components.day == 1{
    return String(components.day) + " Day To Go"
    }
    else if components.day > 1{
    return String(components.day) + " Days To Go"
    }
    
    return "HAPPENED";
    
    }
    */
    
    class func getDays(_ dt:String) -> String {
        if dt == "" {
            return "";
        }
        let timeStr = Double(dt)!/1000
        let endDate = DateHelper.convertTimeStampToDate(String(timeStr));
        let f:DateFormatter = DateFormatter()
        f.timeZone = TimeZone.autoupdatingCurrent
        f.dateFormat = "yyyy-M-dd'T'HH:mm:ss.SSSZZZ"
        
        let now = f.string(from: Date())
        //let startDate = f.dateFromString(endDate)
        let startDate = f.date(from: now)
        let calendar: Calendar = Calendar.current
        
        let calendarUnits:NSCalendar.Unit = [.weekOfMonth, .day, .hour, .minute, .second];
        let dateComponents = (calendar as NSCalendar).components(calendarUnits, from: startDate!, to: endDate, options: NSCalendar.Options())
        
        let weeks = Int(dateComponents.weekOfMonth!)
        let days = Int(dateComponents.day!)
        let hours = Int(dateComponents.hour!)
        let min = Int(dateComponents.minute!)
        // let sec = abs(dateComponents.second)
        
        var timeAgo = ""
        
        if min < 0 {
            timeAgo = "HAPPENED"
        }
        
        if (min > 0){
            if (min > 1) {
                timeAgo = "\(min) Minutes to go"
            } else {
                timeAgo = "\(min) Minute to go"
            }
        }
        
        if(hours > 0){
            if (hours > 1) {
                timeAgo = "\(hours) Hours to go"
            } else {
                timeAgo = "\(hours) Hour to go"
            }
        }
        
        if (days > 0) {
            if (days > 1) {
                timeAgo = "\(days) Days to go"
            } else {
                timeAgo = "\(days) Day to go"
            }
        }
        
        if(weeks > 0){
            if (weeks > 1) {
                timeAgo = "\(weeks) Weeks to go"
            } else {
                timeAgo = "\(weeks) Week to go"
            }
        }
        
        print("timeAgo is===> \(timeAgo)")
        return timeAgo;
    }
    
   
    //Mark: Party Date formula
    class func calulatePartyDate(_ startDt:String, endDate:String) -> String {
        if startDt == "" {
            return "";
        }
        let startTimeStr = Int64(startDt)!/1000
        let endTimeStr = Int64(endDate)!/1000
        let currentTimeStr = Int64(DateHelper.convertDateToTimeStamp(Date()));
        
        if currentTimeStr > endTimeStr {
            return "It's Over";
        }
        else if currentTimeStr > startTimeStr && currentTimeStr < endTimeStr {
            return "On Going";
        }
        else {
            let f:DateFormatter = DateFormatter()
            f.timeZone = TimeZone.autoupdatingCurrent
            f.dateFormat = "yyyy-MM-dd h:mm a"
            
            let now = f.string(from: Date())
            //let startDate = f.dateFromString(endDate)
            let currentDate = f.date(from: now)
            
            print("Start Date :\(startTimeStr)")
            print("End Date :\(endTimeStr)")
            print("Current Date :\(currentTimeStr)")
            let startDate = DateHelper.timeStampToDate1(String(startTimeStr));
            //let endDate = DateHelper.convertTimeStampToDate(String(endTimeStr));
            
            let calendar: Calendar = Calendar.current
            
            let calendarUnits:NSCalendar.Unit = [.weekOfMonth, .day, .hour, .minute];
            let dateComponents = (calendar as NSCalendar).components(calendarUnits, from: startDate, to: currentDate!, options: NSCalendar.Options(rawValue: 0))
            
            let weeks = Int(dateComponents.weekOfMonth!)
            let days = Int(dateComponents.day!)
            let hours = Int(dateComponents.hour!)
            let min = Int(dateComponents.minute!)
            // let sec = abs(dateComponents.second)
            
            var timeAgo = ""
            
            if min < 0 {
                timeAgo = "It's Over"
            }
            
            if (min > 0){
                if (min > 1) {
                    timeAgo = "\(min) Mins"
                } else {
                    timeAgo = "\(min) Min"
                }
            }
            
            if(hours > 0){
                if (hours > 1) {
                    timeAgo = "\(hours) Hrs"
                } else {
                    timeAgo = "\(hours) Hr"
                }
            }
            
            if (days > 0) {
                if (days == 1) {
                    timeAgo = "Go 2Moro"
                } else if days == 2  {
                    timeAgo = "Day After"
                }
                else {
                    timeAgo = DateHelper.convertTimeStampToStringDayMonth("\(Int64(startDt)!/1000)");
                }
            }
            
            if(weeks > 0){
                timeAgo = DateHelper.convertTimeStampToStringDayMonth("\(Int64(startDt)!/1000)");
            }
            
            print("timeAgo is===> \(timeAgo)")
            return timeAgo;
        }
    }
    
    // 03 Dec Format
     func convertTimeStampToStringDayMonth(_ strTimeStamp:String)->String
    {
        let dateFormatter = DateFormatter();
        let _interval = Double(strTimeStamp);
        let date = Date.init(timeIntervalSince1970: _interval!);
        dateFormatter.dateFormat = "dd MMM";
        let finalDate = dateFormatter.string(from: date);
        return finalDate;
    }
    
    
    class func getPartyPastLive(_ partyDate:String)->Bool{
        
        if partyDate == "" {
            return false;
        }
        let endTimeStr = Int64(partyDate)!/1000
        let currentTimeStr = Int64(DateHelper.convertDateToTimeStamp(Date()));
        
        if currentTimeStr > endTimeStr {
            return false;
        } else {
            return true;
        }
        
        
//        if partyDate != "" {
//            let timeStr = Double(partyDate)!/1000
//            let endDate = DateHelper.convertTimeStampToDate(String(timeStr));
//            let cal = NSCalendar.currentCalendar()
//            let unit:NSCalendarUnit = .Day
//            let components = cal.components(unit, fromDate: NSDate(), toDate: endDate, options: NSCalendarOptions())
//            print(components.day)
//            if components.day > 0 || components.day == 0 {
//                return true;
//            } else {
//                return false;
//            }
//        }else {
//            return true;
//        }
        
    }
    
    //Party Spot Manaement Format (Time)
    

    class func convertTimeStampToStrTime(_ strTimeStamp:String, formatStr:String)->String
    {
        let dateFormatter = DateFormatter();
        let _interval = Double(strTimeStamp);
        let date = Date.init(timeIntervalSince1970: _interval!);
        dateFormatter.dateFormat = formatStr //"MM/dd/yyyy";
        let finalDate = dateFormatter.string(from: date);
        return finalDate;
    }


    
   class func dateDifference(_ dt:Date)-> String{
        let secondsPerDay:TimeInterval = 60 * 60 * 24;
        var diff = dt.timeIntervalSinceNow * -1.0;
        // if the difference is negative, then the given date/time is in the future
        // (because we multiplied by -1.0 to make it easier to follow later)
        if (diff < 0){
        return "In the future";
        }
        diff /= secondsPerDay; // get the number of days
        // if the difference is less than 1, the date occurred today, etc.
        if (diff < 1) {
        return "Today";
        }
        else if (diff < 2){
        return "Yesterday";
        }
        else {
        let dateFormatter = DateFormatter();
        dateFormatter.dateFormat = "EEEE, dd MMM";
        return dateFormatter.string(from: dt);
        }
        
    }
    
    class func dateChatDifference(_ dt:Date)-> String{
        let secondsPerDay:TimeInterval = 60 * 60 * 24;
        var diff = dt.timeIntervalSinceNow * -1.0;
        // if the difference is negative, then the given date/time is in the future
        // (because we multiplied by -1.0 to make it easier to follow later)
        if (diff < 0){
            return "In the future";
        }
        diff /= secondsPerDay; // get the number of days
        // if the difference is less than 1, the date occurred today, etc.
        if (diff < 1) {
            let dateFormatter = DateFormatter();
            dateFormatter.dateFormat = "hh:mm a";
            return dateFormatter.string(from: dt);
//            return "Today";
        }
        else if (diff < 2){
            return "Yesterday";
        }
        else {
            let dateFormatter = DateFormatter();
            dateFormatter.dateFormat = "EEE, dd MMM";
            return dateFormatter.string(from: dt);
        }
        
    }

    
    
    func combineDateWithTime(_ date: Date, time: Date) -> Date? {
        let calendar = Calendar.current
        
        let dateComponents = (calendar as NSCalendar).components([.year, .month, .day], from: date)
        let timeComponents = (calendar as NSCalendar).components([.hour, .minute, .second], from: time)
        
        var mergedComponments = DateComponents()
        mergedComponments.year = dateComponents.year
        mergedComponments.month = dateComponents.month
        mergedComponments.day = dateComponents.day
        mergedComponments.hour = timeComponents.hour
        mergedComponments.minute = timeComponents.minute
        mergedComponments.second = timeComponents.second
        
        return calendar.date(from: mergedComponments)
    }
    
   class func getTimein24Hrs(_ strTime:String)-> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        let date = dateFormatter.date(from: strTime)
        dateFormatter.dateFormat = "HH:mm"
        let date24 = dateFormatter.string(from: date!)
        return date24;
    }
    
   class func convertTimeInTimestamp(_ timeVal:String)->String{
        let time = getTimein24Hrs(timeVal);
        let arr = Helper.seperatedByColon(time);
       let hrsMiliSecond = Int((arr[0] as? String)!)!*60*60*1000;
       let minMiliSecond = Int((arr[1] as? String)!)!*60*1000;
        let timeStamp = String(hrsMiliSecond + minMiliSecond);
        return timeStamp;
        
    }
    
   class func getTimeFromTimeStamp(_ strVal:String)->String{
        let milliseconds = Int(strVal)
        let seconds =  (milliseconds! / 1000) % 60 ;
        let minutes =  ((milliseconds! / (1000*60)) % 60);
        let hours   =  ((milliseconds! / (1000*60*60)) % 24);
    
    
    let date: Date = Date()
    let cal: Calendar = Calendar(identifier: .gregorian)
    
    let newDate: Date = cal.date(bySettingHour: hours, minute: minutes, second: seconds, of: date)!
    
//        var dtCompontents = DateComponents()
//        dtCompontents.hour = hours
//        dtCompontents.minute = minutes
//        dtCompontents.second = seconds
//        let calendarDate = Calendar.current.date(from: dtCompontents)!
    
        /***** NSDateFormatter Part *****/
        let time = getStringfromDateforChatting(newDate);
       return time;

    }
    
    class func equalToDate(_ date1: Date,date2:Date) -> Bool {
        
        let dateArray1 = Helper.seperatedByWhiteSpace(String(describing: date1));
        let dateArray2 = Helper.seperatedByWhiteSpace(String(describing: date2));
        if dateArray1[0] as? String == dateArray2[0] as? String {
            return true;
        } else {
            return false;
        }
    }

    


}
