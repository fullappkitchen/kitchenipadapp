//
//  BaseViewController.swift
//  KitchenLogs for iPad
//
//  Created by Manish on 08/09/17.
//  Copyright © 2017 KitchenLogs Ltd. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
typealias completion = (_ clickedTitle:String) -> Void;
    override public var traitCollection: UITraitCollection {
        if UIDevice.current.userInterfaceIdiom == .pad && UIDevice.current.orientation.isPortrait {
            return UITraitCollection(traitsFrom:[UITraitCollection(horizontalSizeClass: .compact), UITraitCollection(verticalSizeClass: .regular)])
        }
        return super.traitCollection
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getAppDelegate()->AppDelegate{
        return (UIApplication.shared.delegate as? AppDelegate)!;
    }
    
    // Alert View Display
    func alert(_ alertTitle:String,alertMessage:String, delegate:UIViewController,firstButton:String,secondButton:String,isSingleOK:Bool, completion: @escaping (_ clickedTitle:String) -> Void ){
        let alertController = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        if isSingleOK == true {
            let noAction = UIAlertAction(title:firstButton, style: .default) { (action) -> Void in
                completion(firstButton);
            }
            alertController.addAction(noAction)
        } else {
            let firstAction = UIAlertAction(title:firstButton, style: .default) { (action) -> Void in
                completion(firstButton);
            }
            let secondAction = UIAlertAction(title:secondButton, style: .default) { (action) -> Void in
                completion(secondButton);
            }
            alertController.addAction(firstAction)
            alertController.addAction(secondAction)
        }
        self.present(alertController, animated: true, completion: nil)
    }

    // Naviagation Method
    func navigateWithMenu(toViewController:String,valAnyObj:AnyObject){
        getAppDelegate().window?.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        
        if toViewController == "HomeCntrl" {
            let mainViewController = getstoryboardName().instantiateViewController(withIdentifier: "HomeCntrl") as! HomeCntrl
            addMenuClass(mainView: mainViewController);
 
            
        }
        else if toViewController == "ViewLogController" {
            let mainViewController = getstoryboardName().instantiateViewController(withIdentifier: "ViewLogController") as! ViewLogController
            addMenuClass(mainView: mainViewController);
            
        }
        
    }
    
    // StoryBoard Name
    func getstoryboardName()-> UIStoryboard{
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        return storyBoard;
    }
    
    // Add Menu With Navigation View
    func addMenuClass(mainView:UIViewController){
        let leftViewController =  MenuViewController(nibName : "MenuViewController" , bundle:  nil)
        let slideMenuController = SlideMenuController(mainViewController: mainView, leftMenuViewController: leftViewController);
        getAppDelegate().sliderView = slideMenuController
        self.navigationController?.pushViewController(slideMenuController, animated: false);
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
