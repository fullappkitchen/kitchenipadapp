//
//  LinkAttributatedLabel.swift
//  GoParties
//
//  Created by Manish on 11/08/17.
//  Copyright © 2017 Shubham . All rights reserved.
//

import UIKit

protocol LinkLabelGastureDelegate:NSObjectProtocol {
    func attributedLabelCallBack(strVal:String)
}
class LinkAttributatedLabel: UILabel {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    typealias CompletionHandler = (_ clickedString:String)-> Void;
    var tapGesture: UITapGestureRecognizer?
    weak var delegate:LinkLabelGastureDelegate?
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.initialization()
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initialization()
    }
    
    func initialization(){
//        let newsString: NSMutableAttributedString = NSMutableAttributedString(string: self.text!)
//        let textRange = NSString(string: self.text!)
//        let substringRange = textRange.range(of: "Terms and Conditions") // You can add here for own specific under line substring
//        newsString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: substringRange)
//        // self.attributedText = newsString.copy() as? NSAttributedString
//        self.attributedText = newsString
        self.tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapResponse))
        self.isUserInteractionEnabled =  true
        self.addGestureRecognizer(tapGesture!)
        
    }
    
    
    
    func tapResponse(recognizer: UITapGestureRecognizer) {
        
        let text = (self.text)!
        let termsRange = (text as NSString).range(of: "TERMS & CONDITIONS")
        let termsRange1 = (text as NSString).range(of: "PRIVACY POLICY")
        if (tapGesture?.didTapAttributedTextInLabel(label: self, inRange: termsRange))! {
            print("TERMS & CONDITIONS")
            self.delegate?.attributedLabelCallBack(strVal: "TERMS & CONDITIONS");
            
        }
        else if (tapGesture?.didTapAttributedTextInLabel(label: self, inRange: termsRange1))! {
            print("PRIVACY POLICY")
            self.delegate?.attributedLabelCallBack(strVal: "PRIVACY POLICY");
            
        }
        else {
            print("Tapped none ")
        }
    }

}


//MARK:UITapGestureRecognizer Extension
//MARK:
extension UITapGestureRecognizer {
    
    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: label.attributedText!)
        
        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)
        
        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize
        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        
        let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x,y:(labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y)
        let locationOfTouchInTextContainer = CGPoint(x:locationOfTouchInLabel.x - textContainerOffset.x,
                                                     y:locationOfTouchInLabel.y - textContainerOffset.y);
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        return NSLocationInRange(indexOfCharacter, targetRange)
    }
    
}
