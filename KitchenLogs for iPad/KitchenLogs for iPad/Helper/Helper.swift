//
//  Helper.swift
//  EasyShop24
//
//  Created by Manish on 6/4/15.
//  Copyright (c) 2015 Nzt. All rights reserved.
//

import UIKit

class Helper: NSObject {
    
    //DISABLE ALL TOUCH
    class func disableAllEventTouch(){
        UIApplication.shared.beginIgnoringInteractionEvents();
    }
    //ENABLE ALL TOUCH
    class func enableAllEventTouch(){
        UIApplication.shared.endIgnoringInteractionEvents();
    }
    
    //OPEN URL IN SAFARI
    class func OpenSafariWithURl(_ urlStr:String){
        let myStringArr = urlStr.components(separatedBy: " ")
        if myStringArr.count > 0{
        let hello: String = myStringArr [0]
        let url = URL(string: hello);
        if (url != nil) {
            
        UIApplication.shared.openURL(URL(string: hello)!);
        }
        }
    }
    
    //SAVE BOOL IN DEFAULT
    class func saveBoolInDefaultkey(_ key:String,state:Bool) {
        UserDefaults.standard.set(state, forKey: key);
        UserDefaults.standard.synchronize();
    }
    
    //FETCH BOOL FROM DEFAULT
    class func fetchBool(_ key:String)->Bool{
        return UserDefaults.standard.bool(forKey: key);
    }
    
    //SAVE STRING IN USER DEFAULT
    class func saveStringInDefault(_ key:String,value:String) {
        UserDefaults.standard.set(value, forKey: key);
        UserDefaults.standard.synchronize();
    }
    
    class func isKeyExists(_ key:String) ->Bool {
        if (UserDefaults.standard.object(forKey: key) != nil) {
            return true
        }else {
            return false
        }
    }
    
    //SAVE OBJECTS IN USER DEFAULT
    class func saveObjectsInDefault(_ key:String,value:AnyObject) {
        let data =  NSKeyedArchiver.archivedData(withRootObject: value);
        UserDefaults.standard.set(data, forKey: key)
        UserDefaults.standard.synchronize();
    }
    
    
    //FETCH OBJECTS FROM USER DEFAULT
    class func fetchObjects(_ key:String)->AnyObject {
        var objData:AnyObject!
        if let data = UserDefaults.standard.object(forKey: key) as? Foundation.Data {
            objData = (NSKeyedUnarchiver.unarchiveObject(with: data))! as AnyObject
            
        }
        return objData;
        
        
    }
    
    
    //FETCH STRING FROM USER DEFAULT
    class func fetchString(_ key:String)->AnyObject {
        if (UserDefaults.standard.object(forKey: key) != nil) {
             return UserDefaults.standard.value(forKey: key)! as AnyObject;
        }else {
            return "" as AnyObject;
        }

       
    }
    
    

    
    // SAVE OBJECT ARRAY IN USER DEFAULT
    class func saveObjectArray(_ key:String, objArray:NSMutableArray){
        let data =  NSKeyedArchiver.archivedData(withRootObject: objArray);
        UserDefaults.standard.set(data, forKey: key)
        UserDefaults.standard.synchronize();
    
    }
    
    // SAVE OBJECT DICTONARY IN USER DEFAULT
    class func saveObjectDict(_ key:String, objArray:NSMutableDictionary){
        let data =  NSKeyedArchiver.archivedData(withRootObject: objArray);
        UserDefaults.standard.set(data, forKey: key)
        UserDefaults.standard.synchronize();
        
    }

    
    // FETCH OBJECT ARRAY FROM USER DEFAULT
    class func fetchObjectArray(_ key:String)->NSMutableArray {
        var objArray:NSMutableArray=NSMutableArray();
        if let data = UserDefaults.standard.object(forKey: key) as? Foundation.Data {
             objArray = (NSKeyedUnarchiver.unarchiveObject(with: data) as? NSMutableArray)!
           
        }
        return objArray;
    }
    
    // FETCH OBJECT DICTIONARY FROM USER DEFAULT
    class func fetchObjectDict(_ key:String)->NSMutableDictionary {
        var objArray:NSMutableDictionary=NSMutableDictionary();
        if let data = UserDefaults.standard.object(forKey: key) as? Foundation.Data {
            objArray = (NSKeyedUnarchiver.unarchiveObject(with: data) as? NSMutableDictionary)!
        }
        return objArray;
    }
    
    //REMOVE NSUSER DEFAULT
    class func removeUserDefault(_ key:String) {
        UserDefaults.standard.removeObject(forKey: key);
    }
    /*
     // SAVE LOGIN CREDITIONALS
    class func saveLoginCreditionals(loginUserObject:User) {
        let data =  NSKeyedArchiver.archivedDataWithRootObject(loginUserObject);
        NSUserDefaults.standardUserDefaults().setObject(data, forKey: LOGINCREDENTIAL)
        NSUserDefaults.standardUserDefaults().synchronize();
    }
    
    // FETCH LOGIN CREDITIONALS
    class func fetchLoginCreditionals()->User {
        let loginUser = User();
        if let data = NSUserDefaults.standardUserDefaults().objectForKey(LOGINCREDENTIAL) as? NSData {
          return (NSKeyedUnarchiver.unarchiveObjectWithData(data) as? User)!;
        }
        return loginUser;
    }
*/
    
  
   // SEARCH DATA WITH PREDICATE
    class func getSearchList(_ searchText:String,searchList:NSMutableArray,searchObject:String) -> NSArray {
        //var predicateFilter = NSPredicate(format:"SELF.%@ beginswith[c] %@",searchObject,searchText)
        let predicateFilter = NSPredicate(format:"SELF.%@ CONTAINS[c] %@",searchObject,searchText)
        let filteredList = searchList.filtered(using: predicateFilter)
        return filteredList as NSArray;
    }
    
    
    // SEARCH DATA WITH PREDICATE
//    class func getSearchListByResult(searchText:String,searchList:Results<City>,searchObject:String) -> Results<City> {
//        let predicate = NSPredicate(format: "%@ BEGINSWITH %@", searchObject,searchText)
//        return tanDogs = realm.objects(Dog).filter(predicate)
//    }

    
    class func pushController(_ cntrl:UIViewController, screenName:String) {
//        if let viewController = NSClassFromString(screenName) as? UIViewController.Type {
//           
//        
//    }
        
        
    }
    
    
    // PASSWORD SECURITY WITH PREDICATE
    class func passwordSecurityCheck(_ password:String) -> Bool {
        let laxString = "^(?=.*[A-Za-z])(?=.*\\d)([$@$!%*#?&]?)[A-Za-z\\d[$@$!%*#?&]?]{6,}$"
        let passwordTest = NSPredicate(format:"SELF MATCHES %@",laxString)
        return passwordTest.evaluate(with: password);
    }

    
    
    // SEPERATED STRING DATA INTO ARRAY
    class func seperatedByCommas(_ strValue:String)->NSArray {
        let array = strValue.components(separatedBy: ",") as NSArray
        return array;
    }
    
    class func seperatedByPlus(_ strValue:String)->NSArray {
        let array = strValue.components(separatedBy: "+") as NSArray
        return array;
    }
    class func seperatedByColon(_ strValue:String)->NSArray {
        let array = strValue.components(separatedBy: ":") as NSArray
        return array;
    }
    
    class func mergeString(_ strValue:String)->String {
        var mergeStr:String = "";
        let array = strValue.components(separatedBy: " ") as NSArray
        for i in 0..<array.count {
            mergeStr = mergeStr + (array[i] as! String);
        }
        return mergeStr;
    }
    
    class func seperatedByWhiteSpace(_ strValue:String)->NSArray {
        let array = strValue.components(separatedBy: " ") as NSArray
        return array;
    }
    
    class func seperatedByWhiteSpaceToNewLine(_ strValue:String)->String {
        let array = strValue.components(separatedBy: " ") as NSArray
        if array.count > 1 {
        return "\(array[0])\n\(array[1])"
        }
        else if array.count > 0 {
            return "\(array[0])"
        }
        return "";
    }
    
    class func seperatedByEnd(_ strValue:String)->NSArray {
        let array = strValue.components(separatedBy: "&") as NSArray
        return array;
    }
    
    class func seperatedByHypen(_ strValue:String)->NSArray {
        let array = strValue.components(separatedBy: "-") as NSArray
        return array;
    }
    
    class func seperatedByEqual(_ strValue:String)->NSArray {
        let array = strValue.components(separatedBy: "=") as NSArray
        return array;
    }
    class func seperatedBySlash(_ strValue:String)->NSArray {
        let array = strValue.components(separatedBy: "/") as NSArray
        return array;
    }
    
    
      
    // VALIDATE EMAIL ADDRESS
   class func isValidEmail(_ testStr:String) -> Bool {
        // println("validate calendar: \(testStr)")
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    // VALIDATE PHONE NUMBER
   class func isvalidatePhone(_ value: String) -> Bool {
        let PHONE_REGEX = "^\\d{3}\\d{3}\\d{4}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
    
    
    // Google Analytic Screen Tracker Code Here...
    class func screenTracker(_ screenName:String){
        #if DEBUG
            print("I'm running in DEBUG mode")
        #else
            print("I'm running in a non-DEBUG mode")
            let trackers = GAI.sharedInstance().defaultTracker
            trackers.set(kGAIScreenName, value: screenName)
            let builder = GAIDictionaryBuilder.createScreenView()
            trackers.send(builder.build() as [AnyHashable: Any])
        #endif
        
    }
    
    // Google Analytic Event Tracker Code Here...
    class func eventTracker(_ category:String,action:String,label:String){
        #if DEBUG
            print("I'm running in DEBUG mode")
        #else
            print("I'm running in a non-DEBUG mode")
            let trackers = GAI.sharedInstance().defaultTracker
            trackers.set(kGAIEventCategory, value: category)
            trackers.set(kGAIEventAction, value: action)
            trackers.set(kGAIEventLabel, value: label)
            let builder = GAIDictionaryBuilder.createScreenView()
            trackers.send(builder.build() as [AnyHashable: Any])
        #endif
        
    }
    

    
    // CHECK NULL VALUE
   class func checkNullValue(_ keyValue: String, dictValue:NSDictionary)->String{
        if dictValue[keyValue] is NSNull {
            return "";
        }
        else{
            return dictValue[keyValue] as! String
        }
    }
    
    // CHECK NULL VALUE
    class func checkNumberValue(_ keyValue: String, dictValue:NSDictionary)->String{
        if dictValue[keyValue] is NSNull || dictValue[keyValue] == nil {
            return "false";
        }
        else{
            if let value = dictValue.object(forKey: keyValue) {
                if value is String {
                    //print("I'm a String")
                    return (dictValue.object(forKey: keyValue) as? String)!
                }
                else {
                    //print("I'm a NSnumber")
                    let strVal = dictValue.object(forKey: keyValue)! as AnyObject;
                    return "\(strVal)";
                }
            }
        }
        return "false"
    }
    
    
    // CHECK NULL VALUE
    class func checkNilValue(_ keyValue: String, dictValue:NSDictionary)-> String{
        if dictValue[keyValue] is NSNull {
            return "";
        }
        else if dictValue[keyValue] == nil {
           return "";
        }
        else if dictValue[keyValue] is NSNumber  {
            let val = dictValue[keyValue] as! NSNumber ;
            return String(describing: val);
        }
        else{
            return dictValue[keyValue] as! String
        }
        }
    
    
    class func isNextedDictParsable(_ keyValue: String, dictValue:NSDictionary)-> Bool{
        if dictValue[keyValue] is NSNull {
            return false;
        }
        else if dictValue[keyValue] == nil {
            return false;
        }else if dictValue[keyValue] is String {
            return false;
        }
        else {
            return true;
        }

    }
    
    
  
    
    // CHECK NULL VALUE
    class func checkDoubleValue(_ keyValue: String, dictValue:NSDictionary)->Double{
        if dictValue[keyValue] is NSNull {
            return 0.0;
        }
        else if dictValue[keyValue] == nil {
            return 0.0;
        }
        else{
            if dictValue[keyValue] is NSNumber {
                let val = dictValue[keyValue] as! NSNumber ;
                return Double(val);
            }else {
                let val = dictValue[keyValue] as! String;
                return Double(val)!;
            }
            
        }
    }

    
    
    // CHECK NULL VALUE
    class func checkDictNilValue(_ keyValue: String, dictValue:NSDictionary)->AnyObject{
        
         if dictValue[keyValue] is NSNull {
            return "" as AnyObject;
        }
        else if dictValue[keyValue] == nil {
            return "" as AnyObject;
        }
        else if dictValue[keyValue] is NSNumber  {
            let val = dictValue[keyValue] as! NSNumber ;
            return String(describing: val) as AnyObject
        }
        else if dictValue[keyValue] is NSDictionary  {
            let val = dictValue[keyValue] as! NSDictionary ;
            return val;
        }
        else if dictValue[keyValue] is NSArray  {
            let val = dictValue[keyValue] as! NSArray ;
            return val;
        }
        else{
            return dictValue[keyValue] as! String as AnyObject
        }
    }

//   class  func nullToNil(_ value : AnyObject) -> AnyObject? {
//        if value is NSNull || value is String{
//            return nil
//        } else {
//            return value
//        }
//    }
    
    
    
    
    // GET YOUTUBE VIDEO ID
    class func getYoutubeID(_ youtubeURL:String)->String {
        if youtubeURL.isEmpty{
       return "";
        }
        else{
            let videoID = URLComponents(string: youtubeURL)?.queryItems?[0].value
            if let videoID = videoID { print(videoID) }
            if (videoID == nil){
                return "";
            }
            else{
            return videoID!;
            }
        }
        
    }
    
    // GET HEIGHT OF DYNAMIC TEXT VIEW
   class  func heightForView(_ text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        
        label.sizeToFit()
        return label.frame.height
    }
    
    
    // EXPENDS UIVIEW ACCORDING TO SUBVIEW DYNAMIC
    class func expendsViewsAccordingViews(_ viewsArray:NSMutableArray)->CGFloat{
        var height: CGFloat = 0
        let mariginHeight:CGFloat = 20;
        var totalViewsheight: CGFloat = 0
        var lastViewEndPointHeight:CGFloat = 0;
        for i in 0 ..< viewsArray.count{
            let vwViews = viewsArray[i] as! UIView;
             height =   getresizeToFitSubviewsHeight(vwViews);
            if i == 0{
            totalViewsheight =  getresizeToFitSubviewsHeight(viewsArray[i] as! UIView);
             vwViews.frame = CGRect(x: vwViews.frame.origin.x, y: vwViews.frame.origin.y, width: vwViews.frame.size.width,height: height+mariginHeight);
             lastViewEndPointHeight = vwViews.frame.origin.y + vwViews.frame.size.height;
            }
            else{
                print("totalViewsheight is :\(lastViewEndPointHeight)");
                vwViews.frame = CGRect(x: vwViews.frame.origin.x, y: lastViewEndPointHeight, width: vwViews.frame.size.width,height: height+mariginHeight);
                lastViewEndPointHeight = vwViews.frame.origin.y + vwViews.frame.size.height;
                totalViewsheight = totalViewsheight + getresizeToFitSubviewsHeight(viewsArray[i] as! UIView);
            }
        }
        return lastViewEndPointHeight;
    }
    
    /// Extension, resizes this view so it fits the largest subview
   class func getresizeToFitSubviewsHeight(_ vwView:UIView)->CGFloat {
        var width: CGFloat = 0
        var height: CGFloat = 0
        for someView in vwView.subviews {
            let aView = someView
            if aView is UILabel {
            let lblText = aView as? UILabel
             print("lblText is:\(String(describing: lblText))")
            }
            let newWidth = aView.frame.origin.x + aView.frame.width
            width = max(width, newWidth)
            height = height + aView.frame.height;
        }
        print("Vw Height is:\(height)")
        return height;
    }
    
    
    // SET PROFILE TYPE VIEW LIKE LIVE DJ, ARTIST,BAND AND DEAL ON PARTY CARD CELL
   class func setProfileTypeViewBorder(_ vwView:UIView)->UIView{
        let vwBorderView = vwView;
        vwBorderView.layer.borderWidth=1
        vwBorderView.layer.borderColor=UIColor(red: 237.0/255.0, green: 139.0/255.0, blue: 38/255.0, alpha: 1.0).cgColor
        vwBorderView.layer.cornerRadius = 2
        return vwBorderView;
    }
    
    
    
    class func  extractYoutubeIdFromLink(_ linkUrl:String)->String{
        let regexString = "((?<=(v|V)/)|(?<=be/)|(?<=(\\?|\\&)v=)|(?<=embed/))([\\w-]++)"
        let regex =  try! NSRegularExpression(pattern: regexString, options: NSRegularExpression.Options.caseInsensitive)
        let matches = regex.matches(in: linkUrl, options:[], range: NSMakeRange(0,linkUrl.characters.count))
        if matches.count > 0{
            if let match = matches.first {
                let result:NSTextCheckingResult = match;
                return linkUrl.substringWithRange(result.range.location, location: 11)
            }
        }
        return "";
    }
    
    
    // Following method is use for the mask the image.
//    class func cropImage(_ img:UIImage,maskImage:UIImage)->UIImage {
//        let maskRef = maskImage.cgImage;
//        let mask = CGImage(maskWidth: (maskRef?.width)!, height: (maskRef?.height)!, bitsPerComponent: (maskRef?.bitsPerComponent)!, bitsPerPixel: (maskRef?.bitsPerPixel)!, bytesPerRow: (maskRef?.bytesPerRow)!, provider: (maskRef?.dataProvider!)!, decode: nil, shouldInterpolate: false);
//        let masked = img.cgImage.masking(mask);
//        return UIImage(cgImage: masked!);
//    }
    
    // FONT IMPLEMENTED FOR WHOLE APP IN LABELS
    
   class func setFontInLabel(_ parentView:UIView){
        for subview in parentView.subviews {
            print("subview Label is:\(subview)")
            if let labelView = subview as? UILabel {
                labelView.font = UIFont(name: "Lato-ThinItalic", size:labelView.font.pointSize);
            }
        }
       
    }

    
    
    // ALL TOUCH HIGHLIGHT LINE
    
   class func changeLineColor(_ lbl:UILabel,isGrayColor:Bool){
        if (isGrayColor == false){
            lbl.backgroundColor = UIColor.getUIColorWithRGB(230, g: 138, b: 0)
        }else
        {
         lbl.backgroundColor = UIColor.getUIColorWithRGB(232, g: 232, b: 233)
        }
        
    }
    
    class func circularView(_ circularView:UIView){
        circularView.layer.cornerRadius = circularView.frame.size.width/2;
        
    }
    
    // Converted Dictionary to Json String
   class func convertDictToJsonSTring(_ dictData:AnyObject)->String{
        let jsonData = try! JSONSerialization.data(withJSONObject: dictData, options: JSONSerialization.WritingOptions.prettyPrinted)
        let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
        print(jsonString)
        return jsonString;
    }
    
    
    
    
    // =======================  All Transition Animations   =====================
    
   class func setTransitionCurlUp(_ view: UIView, hidden: Bool) {
        UIView.transition(with: view, duration: 0.5, options: .transitionCurlUp, animations: {() -> Void in
            view.isHidden = hidden
            }, completion: { _ in })
    }
    
  class func setTransitionCurlDown(_ view: UIView, hidden: Bool) {
        UIView.transition(with: view, duration: 0.5, options: .transitionCurlDown, animations: {() -> Void in
            view.isHidden = hidden
            }, completion: { _ in })
    }
    
    class func setTransitionFlipFromLeft(_ view: UIView, hidden: Bool) {
        UIView.transition(with: view, duration: 0.5, options: .transitionFlipFromLeft, animations: {() -> Void in
            view.isHidden = hidden
            }, completion: { _ in })
    }
    
    class func setTransitionFlipFromRight(_ view: UIView, hidden: Bool) {
        UIView.transition(with: view, duration: 0.5, options: .transitionFlipFromRight, animations: {() -> Void in
            view.isHidden = hidden
            }, completion: { _ in })
    }
    
    
    class func setTransitionFlipFromTop(_ view: UIView, hidden: Bool) {
        UIView.transition(with: view, duration: 0.5, options: .transitionFlipFromTop, animations: {() -> Void in
            view.isHidden = hidden
            }, completion: { _ in })
    }
    
    class func setTransitionFlipFromBottom(_ view: UIView, hidden: Bool) {
        UIView.transition(with: view, duration: 0.5, options: .transitionFlipFromBottom, animations: {() -> Void in
            view.isHidden = hidden
            }, completion: { _ in })
    }
    
    class func setTransitionCrossDissolve(_ view: UIView, hidden: Bool) {
        UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {() -> Void in
            view.isHidden = hidden
            }, completion: { _ in })
    }
    
    class func setTransitionNone(_ view: UIView, hidden: Bool) {
        UIView.transition(with: view, duration: 0.5, options: UIViewAnimationOptions(), animations: {() -> Void in
            view.isHidden = hidden
            }, completion: { _ in })
    }
    
    class func setTransitionEaseIn(_ view: UIView, hidden: Bool) {
        UIView.transition(with: view, duration: 0.5, options: .curveEaseIn, animations: {() -> Void in
            view.isHidden = hidden
            }, completion: { _ in })
    }
    class func setTransitionEaseOut(_ view: UIView, hidden: Bool) {
        UIView.transition(with: view, duration: 0.5, options: .curveEaseOut, animations: {() -> Void in
            view.isHidden = hidden
            }, completion: { _ in })
    }
    //Zooming In
    class func zoomIn(_ vw:UIView){
        vw.isHidden = false;
        UIView.animate(withDuration: 0.5, animations: { () -> Void in
            vw.transform = CGAffineTransform(scaleX: 1.0, y: 1.0);
            }, completion: { (finished:Bool) -> Void in
                vw.transform = CGAffineTransform(scaleX: 1.0, y: 1.0);
                }) }
    //Zooming Out
    class func zoomOut(_ vw:UIView){
        UIView.animate(withDuration: 0.5, animations: { () -> Void in
           vw.transform = CGAffineTransform(scaleX: 0.1, y: 0.1);
            }, completion: { (finished:Bool) -> Void in
         vw.transform = CGAffineTransform(scaleX: 0, y: 0);
                vw.isHidden = true;
        }) }
    
    
    class func stringWhiteSpace(_ str:String)->String{
        let trimmedString = str.trimmingCharacters(
            in: CharacterSet.whitespacesAndNewlines)
        return trimmedString;
    }
    
    
    
    // MARK: Generate QR Code method implementation
    class func generateQRBar(_ strVal:String,imgQRCode:UIImageView){
        var qrcodeImage: CIImage!
        let data = strVal.data(using: String.Encoding.isoLatin1, allowLossyConversion: false)
        let filter = CIFilter(name: "CIQRCodeGenerator")
        filter!.setValue(data, forKey: "inputMessage")
        filter!.setValue("Q", forKey: "inputCorrectionLevel")
        qrcodeImage = filter!.outputImage
    
       let scaleX = imgQRCode.frame.size.width / qrcodeImage.extent.size.width
       let scaleY = imgQRCode.frame.size.height / qrcodeImage.extent.size.height
    
       let transformedImage = qrcodeImage.applying(CGAffineTransform(scaleX: scaleX, y: scaleY))
    
       imgQRCode.image = UIImage(ciImage: transformedImage)
    }
    
    
    
   // Marks: GET QUOTIENT COLORS
    class func getQuatientColor(_ lbl:UILabel, crowdQuotient:String) {
        if crowdQuotient <= "1.0"{
            lbl.backgroundColor = UIColor.getUIColorWithRGB(219, g: 153, b: 87)
        }else if crowdQuotient > "1.0" && crowdQuotient <= "2.0"{
            lbl.backgroundColor = UIColor.getUIColorWithRGB(203, g: 143, b: 124)
        }else if crowdQuotient > "2.0" && crowdQuotient <= "3.0"{
            lbl.backgroundColor = UIColor.getUIColorWithRGB(186, g: 131, b: 151)
        }else if crowdQuotient > "3.0" && crowdQuotient <= "4.0"{
            lbl.backgroundColor = UIColor.getUIColorWithRGB(194, g: 155, b: 193)
        }else if crowdQuotient > "4.0" && crowdQuotient <= "5.0"{
            lbl.backgroundColor = UIColor.getUIColorWithRGB(150, g: 95, b: 184)
        }

    }
    
    
    class func addBlurView(_ inView : UIView)
    {
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = inView.bounds
        blurEffectView.alpha = 0.8;
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        inView.addSubview(blurEffectView)
    }
    

    class func convertDictToNSData(_ dict:NSDictionary)-> Foundation.Data {
        let data : Foundation.Data = NSKeyedArchiver.archivedData(withRootObject: dict)
        return data;
    }
    
    class func convertNSDataToDict(_ data:Foundation.Data)-> NSDictionary {
        if data.count > 10 {
        let dictionary:NSDictionary? = NSKeyedUnarchiver.unarchiveObject(with: data)! as? NSDictionary
        return dictionary!;
        } else {
            let dict = NSDictionary();
            return dict;
        }
    }
    
    



}


//================================== Layout Class for dynamic height ===========================

class HorizontalLayout: UIView {
    
    var xOffsets: [CGFloat] = []
    
    init(height: CGFloat) {
        super.init(frame: CGRect(x: 0, y: 0, width: 0, height: height))
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        
        var width: CGFloat = 0
        
        for i in 0..<subviews.count {
            let view = subviews[i] as UIView
            view.layoutSubviews()
            width += xOffsets[i]
            view.frame.origin.x = width
            width += view.frame.width
        }
        
        self.frame.size.width = width
        
    }
    
    override func addSubview(_ view: UIView) {
        
        xOffsets.append(view.frame.origin.x)
        super.addSubview(view)
        
    }
    
    func removeAll() {
        
        for view in subviews {
            view.removeFromSuperview()
        }
        xOffsets.removeAll(keepingCapacity: false)
        
    }
    
}



class HorizontalFitLayout: HorizontalLayout {
    
    
    override init(height: CGFloat) {
        super.init(height: height)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        
        var width: CGFloat = 0
        var zeroWidthView: UIView?
        
        for i in 0..<subviews.count {
            let view = subviews[i] as UIView
            width += xOffsets[i]
            if view.frame.width == 0 {
                zeroWidthView = view
            } else {
                width += view.frame.width
            }
        }
        
        if width < superview!.frame.width && zeroWidthView != nil {
            zeroWidthView!.frame.size.width = superview!.frame.width - width
        }
        
        super.layoutSubviews()
        
    }
    
}



class VerticalLayout: UIView {
    
    var yOffsets: [CGFloat] = []
    
    init(width: CGFloat) {
        super.init(frame: CGRect(x: 0, y: 0, width: width, height: 0))
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        
        var height: CGFloat = 0
        
        for i in 0..<subviews.count {
            let view = subviews[i] as UIView
            view.layoutSubviews()
            height += yOffsets[i]
            view.frame.origin.y = height
            height += view.frame.height
        }
        
        self.frame.size.height = height
        
    }
    
    override func addSubview(_ view: UIView) {
        
        yOffsets.append(view.frame.origin.y)
        super.addSubview(view)
        
    }
    
    func removeAll() {
        
        for view in subviews {
            view.removeFromSuperview()
        }
        yOffsets.removeAll(keepingCapacity: false)
        
    }
    
}


class VerticalScreenLayout: VerticalLayout {
    
    
    init() {
        super.init(width: UIScreen.main.bounds.width)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        
        self.frame.size.width = UIScreen.main.bounds.width
        super.layoutSubviews()
        
    }
    
}


class VerticalFitLayout: VerticalLayout {
    
    
    override init(width: CGFloat) {
        super.init(width: width)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        
        var height: CGFloat = 0
        var zeroHeightView: UIView?
        
        for i in 0..<subviews.count {
            let view = subviews[i] as UIView
            height += yOffsets[i]
            if view.frame.height == 0 {
                zeroHeightView = view
            } else {
                height += view.frame.height
            }
        }
        
        if height < superview!.frame.height && zeroHeightView != nil {
            zeroHeightView!.frame.size.height = superview!.frame.height - height
        }
        
        super.layoutSubviews()
        
    }
    
}



